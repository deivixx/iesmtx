<?php

namespace App\Repository;

use App\Entity\Hours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Hours|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hours|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hours[]    findAll()
 * @method Hours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hours::class);
    }

    // /**
    //  * @return Hours[] Returns an array of Hours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hours
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findNowBeforeAndAfterAbsences()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT h FROM App\Entity\Hours h WHERE (:now BETWEEN h.begin and h.end) OR (:before BETWEEN DATE_SUB(h.begin, 1, 'HOUR') and DATE_SUB(h.end, 1, 'HOUR')) OR (:after BETWEEN DATE_ADD(h.begin,1,'hour') and DATE_ADD(h.end,1,'hour'))");

        $today = new \DateTime();
        $before = new \DateTime();
        $after = new \DateTime();

        $query->setParameter('now', $today->format("H:i"));
        $query->setParameter('before', $before->sub(new \DateInterval('PT1H'))->format("H:i"));
        $query->setParameter('after', $after->modify('+ 1 hour')->format("H:i"));

        return $query->getResult();
    }


    public function findCloseHours()
    {
        $today = new \DateTime();

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT h FROM App\Entity\Hours h WHERE (:now BETWEEN h.begin and h.end)");
        $query->setParameter('now', $today->format("H:i"));
        $result = $query->getResult();

        if(sizeof($result)>0) {
            $query2 = $em->createQuery("SELECT h FROM App\Entity\Hours h WHERE (h.ord BETWEEN :order - 1 and :order + 1)");
            $query2->setParameter('order', $result[0]->getOrd());

            return $query2->getResult();
        }else{
            return null;
        }

    }
}
