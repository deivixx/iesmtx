<?php

namespace App\Repository;

use App\Entity\TeacherActivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TeacherActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherActivity[]    findAll()
 * @method TeacherActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeacherActivity::class);
    }

    // /**
    //  * @return TeacherActivity[] Returns an array of TeacherActivity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeacherActivity
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function filterTeacherActivitiesQuery($filterValue)
    {
        $em = $this->getEntityManager();

        $date = date_create_from_format('d-m-Y', $filterValue);

        if (!$date) {
            $query = $em->createQuery("select ta from \App\Entity\TeacherActivity ta where ta.description like :filter or ta.begin like :filter or ta.end like :filter order by ta.begin DESC");
            $query->setParameter('filter', '%' . $filterValue . '%');
        } else {
            $query = $em->createQuery("select ta from \App\Entity\TeacherActivity ta where ta.begin like :date or ta.end like :date order by ta.begin DESC");
            $query->setParameter('date', '%' . $date->format('Y-m-d') . '%');
        }
        return $query;
    }


    public function allTeacherActivitiesQuery()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("select ta from \App\Entity\TeacherActivity ta order by ta.begin DESC");

        return $query;
    }

}
