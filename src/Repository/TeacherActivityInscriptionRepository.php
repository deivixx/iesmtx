<?php

namespace App\Repository;

use App\Entity\TeacherActivityInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TeacherActivityInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherActivityInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherActivityInscription[]    findAll()
 * @method TeacherActivityInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherActivityInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeacherActivityInscription::class);
    }

    // /**
    //  * @return TeacherActivityInscription[] Returns an array of TeacherActivityInscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeacherActivityInscription
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
