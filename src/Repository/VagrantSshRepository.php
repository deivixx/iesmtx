<?php

namespace App\Repository;

use App\Entity\VagrantSsh;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VagrantSsh|null find($id, $lockMode = null, $lockVersion = null)
 * @method VagrantSsh|null findOneBy(array $criteria, array $orderBy = null)
 * @method VagrantSsh[]    findAll()
 * @method VagrantSsh[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VagrantSshRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VagrantSsh::class);
    }

    // /**
    //  * @return VagrantSsh[] Returns an array of VagrantSsh objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VagrantSsh
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
