<?php

namespace App\Repository;

use App\Entity\Absence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Absence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Absence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Absence[]    findAll()
 * @method Absence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbsenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Absence::class);
    }

    // /**
    //  * @return Absence[] Returns an array of Absence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Absence
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findTodayAbsences($day)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT a FROM App\Entity\Absence a WHERE (a.finalDay is not null AND :day BETWEEN a.day AND a.finalDay) OR (a.finalDay is null and a.day = :day)");
        $query->setParameter('day', $day->format("Y-m-d"));
        return $query->getResult();
    }


    public function filterAbsencesQuery($filterValue)
    {
        $em = $this->getEntityManager();

        $date = date_create_from_format('d-m-Y', $filterValue);

        if (!$date) {
            $query = $em->createQuery("select a from \App\Entity\Absence a join a.teacher t where t.name like :filter or t.surname like :filter or a.day like :filter or a.finalDay like :filter order by a.day DESC");
            $query->setParameter('filter', '%' . $filterValue . '%');
        } else {
            $query = $em->createQuery("select a from \App\Entity\Absence a join a.teacher t where a.day like :date or a.finalDay like :date order by a.day DESC");
            $query->setParameter('date', $date->format("Y-m-d"));
        }

        return $query;
    }

    public function allAbsencesQuery()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("select a from \App\Entity\Absence a join a.teacher t order by a.day DESC");

        return $query;
    }


    public function mineAbsencesQuery($user)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("select a from \App\Entity\Absence a where a.teacher = :user order by a.day DESC");
        $query->setParameter('user', $user);
        return $query;
    }


    public function save(Absence $absence)
    {
        $em = $this->getEntityManager();

        $em->persist($absence);
        $em->flush();

    }
}