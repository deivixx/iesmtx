<?php

namespace App\Repository;

use App\Entity\ExtraCurricular;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ExtraCurricular|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraCurricular|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraCurricular[]    findAll()
 * @method ExtraCurricular[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraCurricularRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraCurricular::class);
    }

    // /**
    //  * @return ExtraCurricular[] Returns an array of ExtraCurricular objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraCurricular
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
