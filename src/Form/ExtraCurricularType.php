<?php

namespace App\Form;

use App\Entity\ExtraCurricular;
use App\Entity\Teacher;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtraCurricularType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('begin', DateTimeType::class, [
                'date_widget' => 'single_text'
            ])
            ->add('end', DateTimeType::class, [
                'date_widget' => 'single_text'
            ])
            ->add('teachers', EntityType::class, [

                'class' => Teacher::class,
                'choice_label' => 'fullname',
                'multiple' => true,
                //'help' => 'Mantén pulsada la tecla CTRL para añadir varios profesores',
                //'attr' => ['class'=> 'mdb-select colorful-select dropdown-primary md-form', 'searchable' => "Busca..."]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExtraCurricular::class,
        ]);
    }
}
