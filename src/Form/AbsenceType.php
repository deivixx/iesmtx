<?php

namespace App\Form;

use App\Entity\Absence;
use App\Entity\Hours;
use App\Entity\Permissions;
use App\Entity\Teacher;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityRepository;


class AbsenceType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $options['user'];

        $builder->add('day', DateType::class, [

                'label_format' => 'nueva.ausencia.dia',
                'widget' => 'single_text',
            ]
        )
            ->add('work', CheckBoxType::class, [

                    'label_format' => 'nueva.ausencia.trabajo',
                    'required' => false,
                ]
            )
            ->add('commentAlu', TextareaType::class, ['required' => false, 'label_format' => 'nueva.ausencia.comentalu',])
            ->add('commentDir', TextareaType::class, ['required' => false, 'label_format' => 'nueva.ausencia.comentdir',])
            ->add('motivation', EntityType::class, [
                'class' => Permissions::class,
                'choice_label' => 'description',
                'placeholder' => $this->translator->trans('nueva.ausencia.motivo.ph'),

                'label_format' => 'nueva.ausencia.motivo',
            ])
            ->add('hours', EntityType::class, [
                'class' => Hours::class,
                'choice_label' => 'description',
                'multiple' => true,
                'expanded' => true,
                'label_format' => 'nueva.ausencia.horas',
                'attr' => ['class' => 'hours-check'],
            ]);

        if (in_array("ROLE_DIRECTIVA", $user->getRoles())) {

            $builder->add('teacher', EntityType::class, [
                'class' => Teacher::class,
                'choice_label' => 'completeusername',
                'preferred_choices' => array($user)

            ]);
        } else {

            $builder->add('teacher', EntityType::class, [
                'class' => Teacher::class,
                'query_builder' => function (EntityRepository $er) use (&$user) {

                    return $er->createQueryBuilder('tq')
                        ->select('t')
                        ->from('App\Entity\Teacher', 't')
                        ->where('t.id = ?1')
                        ->setParameter(1, $user->getId());
                },
                'choice_label' => 'completeusername',
                'disabled' => true,
            ]);
        }


        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $absence = $event->getData();
            $form = $event->getForm();
            if ((!$absence || null === $absence->getId()) || ($absence != null && $absence->getFinalDay() == null)) {
                $form->add('type', ChoiceType::class, [
                    'choices' => [$this->translator->trans('nueva.ausencia.dia') => "day", $this->translator->trans('nueva.ausencia.periodo') => "period"],

                    'label_format' => 'nueva.ausencia.tipo',
                    'expanded' => true,
                    'attr' => ['class' => 'form-check-inline'],
                    'data' => "day",
                ]);

                $form->add('finalDay', DateType::class, [

                        'label_format' => 'nueva.ausencia.hasta',
                        'label_attr' => ['hidden' => 'true'],
                        'widget' => 'single_text',
                        'required' => false,
                        'attr' => ['hidden' => 'true'],
                    ]
                );
            } elseif ($absence != null && $absence->getFinalDay() != null) {
                $form->add('type', ChoiceType::class, [
                    'choices' => [$this->translator->trans('nueva.ausencia.dia') => "day", $this->translator->trans('nueva.ausencia.periodo') => "period"],

                    'label_format' => 'nueva.ausencia.tipo',
                    'expanded' => true,
                    'attr' => ['class' => 'form-check-inline'],
                    'data' => "period",
                ]);

                $form->add('finalDay', DateType::class, [

                        'label_format' => 'nueva.ausencia.hasta',
                        'widget' => 'single_text',
                        'required' => true,
                    ]
                );
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Absence::class,
            'user' => null
        ]);
    }
}
