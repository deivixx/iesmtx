<?php

namespace App\Form;

use App\Entity\Teacher;
use App\Entity\TeacherActivityInscription;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeacherActivityInscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('teacher', EntityType::class, [

                'class' => Teacher::class,
                'choice_label' => 'completeusername',
                'label' => false,
                'placeholder' => 'Selecciona profesor',

            ])
            ->add('comment', TextType::class, [
                'label' => false,
                'attr' => ['placeholder'=> 'Comment'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TeacherActivityInscription::class,
        ]);
    }
}
