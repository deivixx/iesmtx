<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200120090245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vagrant_ssh (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT NULL, CHANGE final_day final_day DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE teacher ADD email VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON NOT NULL, CHANGE pic pic VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE vagrant_ssh');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT \'NULL\', CHANGE final_day final_day DATE DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE teacher DROP email, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE pic pic VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
