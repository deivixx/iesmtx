<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200129092658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, max_qualif_date DATETIME NOT NULL, date_and_time DATETIME NOT NULL, place VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meetings (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, date_and_time DATETIME NOT NULL, place VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meetings_teacher (meetings_id INT NOT NULL, teacher_id INT NOT NULL, INDEX IDX_33EA1BCA1EAF2177 (meetings_id), INDEX IDX_33EA1BCA41807E1D (teacher_id), PRIMARY KEY(meetings_id, teacher_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE meetings_teacher ADD CONSTRAINT FK_33EA1BCA1EAF2177 FOREIGN KEY (meetings_id) REFERENCES meetings (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meetings_teacher ADD CONSTRAINT FK_33EA1BCA41807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT NULL, CHANGE final_day final_day DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE teacher CHANGE roles roles JSON NOT NULL, CHANGE pic pic VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE meetings_teacher DROP FOREIGN KEY FK_33EA1BCA1EAF2177');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE meetings');
        $this->addSql('DROP TABLE meetings_teacher');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT \'NULL\', CHANGE final_day final_day DATE DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE teacher CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE pic pic VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
