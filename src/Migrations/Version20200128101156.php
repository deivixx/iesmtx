<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200128101156 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE extra_curricular (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, begin DATETIME NOT NULL, end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra_curricular_teacher (extra_curricular_id INT NOT NULL, teacher_id INT NOT NULL, INDEX IDX_E4069EAB5C930E2A (extra_curricular_id), INDEX IDX_E4069EAB41807E1D (teacher_id), PRIMARY KEY(extra_curricular_id, teacher_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE extra_curricular_teacher ADD CONSTRAINT FK_E4069EAB5C930E2A FOREIGN KEY (extra_curricular_id) REFERENCES extra_curricular (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE extra_curricular_teacher ADD CONSTRAINT FK_E4069EAB41807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT NULL, CHANGE final_day final_day DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE teacher CHANGE roles roles JSON NOT NULL, CHANGE pic pic VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE extra_curricular_teacher DROP FOREIGN KEY FK_E4069EAB5C930E2A');
        $this->addSql('DROP TABLE extra_curricular');
        $this->addSql('DROP TABLE extra_curricular_teacher');
        $this->addSql('ALTER TABLE absence CHANGE motivation_id motivation_id INT DEFAULT NULL, CHANGE work work TINYINT(1) DEFAULT \'NULL\', CHANGE final_day final_day DATE DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE charges CHANGE directive directive TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE teacher CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE pic pic VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
