<?php

namespace App\Security\Voter;

use App\Entity\Teacher;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;


class TeacherVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['CAN_EDIT', 'CAN_ADD_OTHER'])
            && $subject instanceof \App\Entity\Teacher;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $teacher = $subject;

        switch ($attribute) {
            case 'CAN_EDIT':
                return $this->canEdit($teacher, $user);
                break;
            case 'CAN_ADD_OTHER':
                return $this->canAddOther($user);
                break;
        }

        return false;
    }



    private function canEdit(Teacher $teacher, Teacher $user)
    {
        if ($this->security->isGranted('ROLE_DIRECTIVA')) {
            return true;
        } else {
            if ($user === $teacher) {
                return true;
            }
        }

        return false;

    }

    private function canAddOther(Teacher $user)
    {
        if ($this->security->isGranted('ROLE_DIRECTIVA')) {
            return true;
        }

        return false;
    }
}
