<?php

namespace App\DataFixtures;

use App\Entity\Teacher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TeacherFixtures extends Fixture
{
	 
	 private $passwordEncoder;

     public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
         $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        $teacher = new Teacher();
        $teacher->setDni('48363889H');
		$teacher->setName('David');
		$teacher->setSurname('Alcaraz Pérez');
        
		$teacher->setPassword($this->passwordEncoder->encodePassword(
             $teacher,
             '1950'
         ));
		 
		 $manager->persist($teacher);
		 $manager->flush();


        $teacher = new Teacher();
        $teacher->setDni('99999999R');
        $teacher->setName('Jaime');
        $teacher->setSurname('Cantero');

        $teacher->setPassword($this->passwordEncoder->encodePassword(
            $teacher,
            '1950'
        ));

        $roles[] = 'ROLE_PROFESOR';
        $roles[1] = 'ROLE_DIRECTIVA';
        $teacher->setRoles($roles);


        $manager->persist($teacher);
        $manager->flush();


        $teacher = new Teacher();
        $teacher->setDni('73360384K');
        $teacher->setName('Pepe');
        $teacher->setSurname('Rico Pobre');

        $teacher->setPassword($this->passwordEncoder->encodePassword(
            $teacher,
            '1950'
        ));

        $manager->persist($teacher);
        $manager->flush();

    }

}
