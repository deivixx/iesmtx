<?php

namespace App\DataFixtures;

use App\Entity\Permissions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PermissionFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
         $permission = new Permissions();
         $permission->setDescription("Enfermedad común (Sin baja médica)");
		 $manager->persist($permission);
		 $manager->flush();


        $permission = new Permissions();
        $permission->setDescription("Actividad extraescolar");
        $manager->persist($permission);
        $manager->flush();

        $permission = new Permissions();
        $permission->setDescription("Baja médica");
        $manager->persist($permission);
        $manager->flush();
    }
	
}
