<?php

namespace App\DataFixtures;

use App\Entity\Hours;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HoursFixtures extends Fixture
{
	 
	 
    public function load(ObjectManager $manager)
    {
        /*
        $hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

        $hours->setDescription('Todo el día');
		$hours->setBegin($begin->setTime(8,0,0));
		$hours->setEnd($end->setTime(15,05,0));
		
		$manager->persist($hours);
		$manager->flush();
		*/
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
        $hours->setDescription('1ª');
		$hours->setBegin($begin->setTime(8,0,0));
		$hours->setEnd($end->setTime(8,55,0));
        $hours->setOrd(1);

		$manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('2ª');
		$hours->setBegin($begin->setTime(8,55,0));
		$hours->setEnd($end->setTime(9,50,0));
        $hours->setOrd(2);

		$manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('3ª');
		$hours->setBegin($begin->setTime(9,50,0));
		$hours->setEnd($end->setTime(10,45,0));
        $hours->setOrd(3);

        $manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('Patio 1');
		$hours->setBegin($begin->setTime(10,45,0));
		$hours->setEnd($end->setTime(11,05,0));
        $hours->setOrd(4);

        $manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('4ª');
		$hours->setBegin($begin->setTime(11,05,0));
		$hours->setEnd($end->setTime(12,0,0));
        $hours->setOrd(5);

        $manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('5ª');
		$hours->setBegin($begin->setTime(12,0,0));
		$hours->setEnd($end->setTime(12,55,0));
        $hours->setOrd(6);

        $manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('Patio 2');
		$hours->setBegin($begin->setTime(12,55,0));
		$hours->setEnd($end->setTime(13,15,0));
        $hours->setOrd(7);

        $manager->persist($hours);
		$manager->flush();		
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('6ª');
		$hours->setBegin($begin->setTime(13,15,0));
		$hours->setEnd($end->setTime(14,10,0));
        $hours->setOrd(8);

        $manager->persist($hours);
		$manager->flush();
		
		$hours = new Hours();
		$begin = new \DateTime();
		$end = new \DateTime();

		
		$hours->setDescription('7ª');
		$hours->setBegin($begin->setTime(14,10,0));
		$hours->setEnd($end->setTime(15,5,0));
        $hours->setOrd(9);

        $manager->persist($hours);
		$manager->flush();

    }
	
}
