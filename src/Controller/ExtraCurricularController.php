<?php

namespace App\Controller;

use App\Entity\ExtraCurricular;
use App\Form\ExtraCurricularType;
use App\Repository\ExtraCurricularRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extra")
 */
class ExtraCurricularController extends AbstractController
{
    /**
     * @Route("/", name="extra_curricular_index", methods={"GET"})
     */
    public function index(ExtraCurricularRepository $extraCurricularRepository): Response
    {
        return $this->render('extra_curricular/index.html.twig', [
            'extra_curriculars' => $extraCurricularRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="extra_curricular_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $extraCurricular = new ExtraCurricular();
        $form = $this->createForm(ExtraCurricularType::class, $extraCurricular);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($extraCurricular);
            $entityManager->flush();



            return $this->redirectToRoute('extra_curricular_index');
        }

        return $this->render('extra_curricular/new.html.twig', [
            'extra_curricular' => $extraCurricular,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_curricular_show", methods={"GET"})
     */
    public function show(ExtraCurricular $extraCurricular): Response
    {
        return $this->render('extra_curricular/show.html.twig', [
            'extra_curricular' => $extraCurricular,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="extra_curricular_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ExtraCurricular $extraCurricular): Response
    {
        $form = $this->createForm(ExtraCurricularType::class, $extraCurricular);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('extra_curricular_index');
        }

        return $this->render('extra_curricular/edit.html.twig', [
            'extra_curricular' => $extraCurricular,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_curricular_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExtraCurricular $extraCurricular): Response
    {
        if ($this->isCsrfTokenValid('delete'.$extraCurricular->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($extraCurricular);
            $entityManager->flush();
        }

        return $this->redirectToRoute('extra_curricular_index');
    }
}
