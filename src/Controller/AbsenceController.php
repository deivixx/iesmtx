<?php

namespace App\Controller;

use App\Entity\Absence;
use App\Form\AbsenceType;
use App\Repository\AbsenceRepository;
use App\Repository\HoursRepository;
use App\Service\AbsenceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Knp\Component\Pager\Paginator;

/**
 * @Route("/absence")
 */
class AbsenceController extends AbstractController
{


    /**
     * @Route("/timetable", name="absence_timetable", methods={"GET"})
     */
    public function timetable(HoursRepository $hoursRepository): Response
    {

        return $this->render('absence/timetable.html.twig', [
            'hours' => $hoursRepository->findAll()
        ]);
    }


    /**
     * @Route("/teachers", name="absence_teachers", methods={"GET"})
     */
    public function teachers(HoursRepository $hoursRepository): Response
    {

        return $this->render('absence/teachers.html.twig', [
            'hours' => $hoursRepository->findCloseHours()
        ]);
    }


    /**
     * @Route("/quadrant", name="absence_quadrant", methods={"GET"})
     */
    public function quadrant(HoursRepository $hoursRepository): Response
    {

        return $this->render('absence/quadrant.html.twig', [
            'hours' => $hoursRepository->findAll()
        ]);
    }


    /**
     * @Route("/timetableasync", name="absence_timetable_async", methods={"GET"})
     */
    public function timetableAsync(HoursRepository $hoursRepository): Response
    {
        return $this->render('absence/timetable_async.html.twig', [
            'hours' => $hoursRepository->findAll()
        ]);
    }


    /**
     * @Route("/", name="absence_index", methods={"GET"})
     */
    public function index(AbsenceRepository $absenceRepository, \Knp\Component\Pager\PaginatorInterface $paginator, Request $request): Response
    {

        if ($request->query->get('filterValue')) {
            $filterValue = $request->query->get('filterValue', "");
            $query = $absenceRepository->filterAbsencesQuery($filterValue);
        } else {
            $query = $absenceRepository->allAbsencesQuery();
        }

        //$paginator = $this->get('knp_paginator');
        $absences = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('absence/index.html.twig', [
            'absences' => $absences,
        ]);
    }


    /**
     * @Route("/mine", name="absence_mine", methods={"GET"})
     */
    public function mine(AbsenceRepository $absenceRepository, \Knp\Component\Pager\PaginatorInterface $paginator, Request $request): Response
    {
        $query = $absenceRepository->mineAbsencesQuery($this->getUser());
        //$paginator = $this->get('knp_paginator');
        $absences = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('absence/mine.html.twig', [
            'absences' => $absences,
        ]);
    }


    /**
     * @Route("/today", name="absence_today", methods={"GET"})
     */
    public function today(AbsenceRepository $absenceRepository): Response
    {
        $today = new \DateTime();
        return $this->render('absence/today.html.twig', [
            'absences' => $absenceRepository->findTodayAbsences($today),
        ]);
    }


    /**
     * @Route("/{id}/proof", name="absence_proof", methods={"GET","POST"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function proof(Request $request, Absence $absence): Response
    {

        $this->denyAccessUnlessGranted('CAN_VIEW', $absence);

        $response = $this->forward('App\Controller\PdfController::proof', [
            'absence' => $absence,
        ]);


        return $response;

    }

    /**
     * @Route("/new", name="absence_new", methods={"GET","POST"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function new(Request $request, AbsenceRepository $absenceRepository): Response
    {
        $absence = new Absence();
        $form = $this->createForm(AbsenceType::class, $absence, array('user' => $this->getUser()));
        $form->handleRequest($request);
        $absence->setTeacher($this->getUser());

        if ($form->isSubmitted() && $form->isValid()) {

            if ($absence->getType() == "day") $absence->setFinalDay(null);

            $absenceRepository->save($absence);

            $this->addFlash(
                'notice', 'Ausencia creada correctamente'
            );

            return $this->redirectToRoute('absence_index');
        }

        return $this->render('absence/new.html.twig', [
            'absence' => $absence,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="absence_show", methods={"GET"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function show(Absence $absence): Response
    {
        try {
            $this->denyAccessUnlessGranted('CAN_VIEW', $absence);

            return $this->render('absence/show.html.twig', [
                'absence' => $absence,
            ]);
        } catch (\Exception $e) {
            $message = null;
            if ($e->getCode() == 403) {
                $message = "No tienes permiso para ver la ausencia";
            } elseif ($e->getCode() == 404) {
                $message = "No se ha encontrado la ausencia solicitada";
            }

            $this->addFlash(
                'error', $message
            );

            return $this->redirectToRoute('absence_index');
        }

    }

    /**
     * @Route("/{id}/edit", name="absence_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function edit(Request $request, Absence $absence): Response
    {
        try {
            $this->denyAccessUnlessGranted('CAN_EDIT', $absence);

            $form = $this->createForm(AbsenceType::class, $absence, array('user' => $this->getUser()));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($absence->getType() == "day") $absence->setFinalDay(null);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'notice', 'Ausencia actualizada correctamente'
                );

                return $this->redirectToRoute('absence_index');
            }

            return $this->render('absence/edit.html.twig', [
                'absence' => $absence,
                'form' => $form->createView(),
            ]);
        } catch (\Exception $e) {

            $message = null;
            if ($e->getCode() == 403) {
                $message = "No tienes permiso para editar la ausencia";
            } elseif ($e->getCode() == 404) {
                $message = "No se ha encontrado la ausencia solicitada";
            }

            $this->addFlash(
                'error', $message
            );

            return $this->redirectToRoute('absence_index');
        }
    }

    /**
     * @Route("/{id}", name="absence_delete", methods={"DELETE"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function delete(Request $request, Absence $absence): Response
    {
        try {
            $this->denyAccessUnlessGranted('CAN_EDIT', $absence);

            if ($this->isCsrfTokenValid('delete' . $absence->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($absence);
                $entityManager->flush();
            }

            $this->addFlash(
                'notice', 'Ausencia eliminada correctamente'
            );

            return $this->redirectToRoute('absence_index');

        } catch (\Exception $e) {

            $message = null;
            if ($e->getCode() == 403) {
                $message = "No tienes permiso para eliminar la ausencia";
            } elseif ($e->getCode() == 404) {
                $message = "No se ha encontrado la ausencia solicitada";
            }

            $this->addFlash(
                'error', $message
            );

            return $this->redirectToRoute('absence_index');
        }
    }


    /**
     * @Route("/{id}/htmlproof", name="absence_html_proof", methods={"GET"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function htmlproof(Request $request, Absence $absence): Response
    {

        return $this->render('absence/proof.html.twig', [
            'absence' => $absence,
        ]);
    }

}
