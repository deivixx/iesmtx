<?php

namespace App\Controller;

use App\Entity\Teacher;
use App\Form\ChangePasswordType;
use App\Form\TeacherType;
use App\Repository\AbsenceRepository;
use App\Repository\TeacherRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/teacher")
 */
class TeacherController extends AbstractController
{
    /**
     * @Route("/", name="teacher_index", methods={"GET"})
     */
    public function index(TeacherRepository $teacherRepository,\Knp\Component\Pager\PaginatorInterface $paginator, Request $request): Response
    {

        if($request->query->get('filterValue')){
            $filterValue = $request->query->get('filterValue', "");
            $query = $this->getDoctrine()->getManager()->createQuery("select t from \App\Entity\Teacher t where t.name like :filter or t.surname like :filter order by t.surname ASC");
            $query->setParameter('filter','%'.$filterValue.'%');
        }else{
            $query = $this->getDoctrine()->getManager()->createQuery("select t from \App\Entity\Teacher t order by t.surname ASC");
        }

        //$paginator = $this->get('knp_paginator');
        $teachers = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('teacher/index.html.twig', [
            'teachers' => $teachers,
        ]);

    }

    /**
     * @Route("/new", name="teacher_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $teacher = new Teacher();
        $form = $this->createForm(TeacherType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($teacher);
            $entityManager->flush();

            return $this->redirectToRoute('teacher_index');
        }

        return $this->render('teacher/new.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="teacher_show", methods={"GET"})
     */
    public function show(Teacher $teacher): Response
    {
        return $this->render('teacher/show.html.twig', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="teacher_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Teacher $teacher): Response
    {
        $this->denyAccessUnlessGranted('CAN_EDIT', $teacher);


        $form = $this->createForm(TeacherType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('teacher_index');
        }

        return $this->render('teacher/edit.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/change_password", name="teacher_change_password", methods={"GET","POST"})
     */
    public function changePassword(Request $request, Teacher $teacher, UserPasswordEncoderInterface $passwordEncoder, TeacherRepository $teacherRepository): Response
    {
        $this->denyAccessUnlessGranted('CAN_EDIT', $teacher);

        if ($request->getMethod() == 'POST') {
            $oldTeacher = $teacherRepository->findOneBy(['id' => $teacher->getId()]);
            $oldFormPassword = $request->request->get('change_password')['old_password'];

            if (!$passwordEncoder->isPasswordValid($oldTeacher, $oldFormPassword)) {
                throw new \Exception('La contraseña aportada no es válida');
            }
        }

        $form = $this->createForm(ChangePasswordType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $teacher->setPassword($passwordEncoder->encodePassword(
                $teacher,
                $form->get('password')->getData()
            ));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('teacher_index');
        }

        return $this->render('teacher/change_password.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="teacher_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Teacher $teacher): Response
    {
        $this->denyAccessUnlessGranted('CAN_EDIT', $teacher);

        if ($this->isCsrfTokenValid('delete' . $teacher->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($teacher);
            $entityManager->flush();
        }

        return $this->redirectToRoute('teacher_index');
    }
}
