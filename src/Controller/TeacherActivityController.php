<?php

namespace App\Controller;

use App\Entity\TeacherActivity;
use App\Form\TeacherActivityType;
use App\Repository\TeacherActivityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/teacher/activity")
 */
class TeacherActivityController extends AbstractController
{
    /**
     * @Route("/", name="teacher_activity_index", methods={"GET"})
     */
    public function index(TeacherActivityRepository $teacherActivityRepository, \Knp\Component\Pager\PaginatorInterface $paginator, Request $request): Response
    {
        if ($request->query->get('filterValue')) {
            $filterValue = $request->query->get('filterValue', "");
            $query = $teacherActivityRepository->filterTeacherActivitiesQuery($filterValue);
        } else {
            $query = $teacherActivityRepository->allTeacherActivitiesQuery();
        }

        //$paginator = $this->get('knp_paginator');
        $teacherActivities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('teacher_activity/index.html.twig', [
            'teacher_activities' => $teacherActivities,
        ]);
    }

    /**
     * @Route("/new", name="teacher_activity_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $teacherActivity = new TeacherActivity();
        $form = $this->createForm(TeacherActivityType::class, $teacherActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($teacherActivity);
            $entityManager->flush();

            return $this->redirectToRoute('teacher_activity_index');
        }

        return $this->render('teacher_activity/new.html.twig', [
            'teacher_activity' => $teacherActivity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="teacher_activity_show", methods={"GET"})
     */
    public function show(TeacherActivity $teacherActivity): Response
    {
        return $this->render('teacher_activity/show.html.twig', [
            'teacher_activity' => $teacherActivity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="teacher_activity_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TeacherActivity $teacherActivity): Response
    {
        $originalInscriptions = new ArrayCollection();
        foreach ($teacherActivity->getInscriptions() as $inscription) {
            $originalInscriptions->add($inscription);
        }


        $form = $this->createForm(TeacherActivityType::class, $teacherActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($originalInscriptions as $inscription) {
                if (false === $teacherActivity->getInscriptions()->contains($inscription)) {
                    // remove the Inscription
                    //$inscription->getTeacherActivity()->removeElement($teacherActivity);
                    $teacherActivity->removeInscription($inscription);

                    // if it was a many-to-one relationship, remove the relationship like this
                    $inscription->setTeacherActivity(null);

                    $this->getDoctrine()->getManager()->persist($inscription);

                    // if you wanted to delete the Tag entirely, you can also do that
                    $this->getDoctrine()->getManager()->remove($inscription);

                    $this->getDoctrine()->getManager()->flush();
                }
            }

            $this->getDoctrine()->getManager()->persist($teacherActivity);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('teacher_activity_index');
        }

        return $this->render('teacher_activity/edit.html.twig', [
            'teacher_activity' => $teacherActivity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="teacher_activity_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TeacherActivity $teacherActivity): Response
    {
        if ($this->isCsrfTokenValid('delete' . $teacherActivity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($teacherActivity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('teacher_activity_index');
    }
}
