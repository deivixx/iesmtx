<?php

namespace App\Controller;

use App\Entity\Absence;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * @Route("/pdf")
 */
class PdfController extends Controller
{


    /**
     * @Route("/proof", name="pdf_proof", methods={"GET"})
     * @IsGranted("ROLE_PROFESOR")
     */
    public function proof(Request $request, Absence $absence): Response
    {

        $url = $this->renderView('absence/proof.html.twig', [
            'absence' => $absence,
        ]);

        $filename = sprintf('justificante-%s.pdf', $absence->getDay()->format('d-m-Y'));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHTML($url, [

                'images' => true,
                //   'enable-javascript' => true,
                'page-size' => 'A4',
                'viewport-size' => '1280x1024',

            ]),
            $filename

        );

    }


}
