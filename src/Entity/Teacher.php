<?php

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TeacherRepository")
 * @UniqueEntity(fields={"dni"}, message="There is already an account with this dni")
 * @Vich\Uploadable
 */
class Teacher implements UserInterface
{

    public function __construct()
    {
        $this->absences = new ArrayCollection();
        $this->extraCurriculars = new ArrayCollection();
        $this->meetings = new ArrayCollection();
        $this->teacherActivityInscriptions = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $dni;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Absence", mappedBy="teacher", orphanRemoval=true)
     */
    private $absences;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="teacher_image", fileNameProperty="pic")
     *
     * @var File
     */
    private $imageFile;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     *
     */
    private $pic;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ExtraCurricular", mappedBy="teachers")
     */
    private $extraCurriculars;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Meetings", mappedBy="teachers")
     */
    private $meetings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TeacherActivityInscription", mappedBy="teacher")
     */
    private $teacherActivityInscriptions;

    /**
     *
     * @param File|UploadedFile $imageFile
     * @throws Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->dni;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_PROFESOR';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPic(): ?string
    {
        return $this->pic;
    }

    public function setPic(?string $pic): self
    {
        $this->pic = $pic;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCompleteUserName()
    {
        return $this->getUsername().' - '.$this->getFullName();
    }

    public function getFullName()
    {
        return $this->name.', '.$this->surname;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }


    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setMotivation($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->contains($absence)) {
            $this->absences->removeElement($absence);
            // set the owning side to null (unless already changed)
            if ($absence->getMotivation() === $this) {
                $absence->setMotivation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExtraCurricular[]
     */
    public function getExtraCurriculars(): Collection
    {
        return $this->extraCurriculars;
    }

    public function addExtraCurricular(ExtraCurricular $extraCurricular): self
    {
        if (!$this->extraCurriculars->contains($extraCurricular)) {
            $this->extraCurriculars[] = $extraCurricular;
            $extraCurricular->addTeacher($this);
        }

        return $this;
    }

    public function removeExtraCurricular(ExtraCurricular $extraCurricular): self
    {
        if ($this->extraCurriculars->contains($extraCurricular)) {
            $this->extraCurriculars->removeElement($extraCurricular);
            $extraCurricular->removeTeacher($this);
        }

        return $this;
    }

    /**
     * @return Collection|Meetings[]
     */
    public function getMeetings(): Collection
    {
        return $this->meetings;
    }

    public function addMeeting(Meetings $meeting): self
    {
        if (!$this->meetings->contains($meeting)) {
            $this->meetings[] = $meeting;
            $meeting->addTeacher($this);
        }

        return $this;
    }

    public function removeMeeting(Meetings $meeting): self
    {
        if ($this->meetings->contains($meeting)) {
            $this->meetings->removeElement($meeting);
            $meeting->removeTeacher($this);
        }

        return $this;
    }

    /**
     * @return Collection|TeacherActivityInscription[]
     */
    public function getTeacherActivityInscriptions(): Collection
    {
        return $this->teacherActivityInscriptions;
    }

    public function addTeacherActivityInscription(TeacherActivityInscription $teacherActivityInscription): self
    {
        if (!$this->teacherActivityInscriptions->contains($teacherActivityInscription)) {
            $this->teacherActivityInscriptions[] = $teacherActivityInscription;
            $teacherActivityInscription->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherActivityInscription(TeacherActivityInscription $teacherActivityInscription): self
    {
        if ($this->teacherActivityInscriptions->contains($teacherActivityInscription)) {
            $this->teacherActivityInscriptions->removeElement($teacherActivityInscription);
            // set the owning side to null (unless already changed)
            if ($teacherActivityInscription->getTeacher() === $this) {
                $teacherActivityInscription->setTeacher(null);
            }
        }

        return $this;
    }
}
