<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HoursRepository")
 */
class Hours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Absence", mappedBy="hours")
     */
    private $absences;

    /**
     * @ORM\Column(type="time")
     */
    private $begin;

    /**
     * @ORM\Column(type="time")
     */
    private $end;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ord;

    public function __construct()
    {
        $this->absences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }


    /**
     * @return Collection|Absence[]
     */
    public function getTodayAbsences(): Collection
    {
        return $this->getAbsences()->filter(function (Absence $absenceList) {
            $today = new \DateTime();

            if ($absenceList->getFinalDay() == null) {
                return $absenceList->getDay()->format("Y-m-d") == $today->format("Y-m-d");
            } else {
                return $today->format("Y-m-d") >= $absenceList->getDay()->format("Y-m-d") and $today->format("Y-m-d") <= $absenceList->getFinalDay()->format("Y-m-d");
            }

        });
    }


    public function addFalta(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->addHour($this);
        }

        return $this;
    }

    public function removeFalta(Absence $absence): self
    {
        if ($this->absences->contains($absence)) {
            $this->absences->removeElement($absence);
            $absence->removeHour($this);
        }

        return $this;
    }

    public function getBegin(): ?\DateTimeInterface
    {
        return $this->begin;
    }

    public function setBegin(\DateTimeInterface $begin): self
    {
        $this->begin = $begin;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getOrd(): ?int
    {
        return $this->ord;
    }

    public function setOrd(?int $ord): self
    {
        $this->ord = $ord;

        return $this;
    }
}
