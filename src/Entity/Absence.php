<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AbsenceRepository")
 */
class Absence
{
    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teacher", inversedBy="absences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacher;

    /**
     * @ORM\Column(type="date")
     */
    private $day;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hours", inversedBy="absences")
     * @ORM\JoinTable(name="absence_hours")
     */
    private $hours;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $work;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentAlu;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentDir;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Permissions", inversedBy="absences")
     */
    private $motivation;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $finalDay;

    public function __construct()
    {
        $this->hours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeacher(): ?Teacher
    {
        return $this->teacher;
    }

    public function setTeacher(?Teacher $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getDay(): ?\DateTimeInterface
    {
        return $this->day;
    }

    public function setDay(\DateTimeInterface $day): self
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @return Collection|Hours[]
     */
    public function getHours(): Collection
    {
        return $this->hours;
    }

    public function addHour(Hours $hour): self
    {
        if (!$this->hours->contains($hour)) {
            $this->hours[] = $hour;
        }

        return $this;
    }

    public function removeHour(Hours $hour): self
    {
        if ($this->hours->contains($hour)) {
            $this->hours->removeElement($hour);
        }

        return $this;
    }

    public function getWork(): ?bool
    {
        return $this->work;
    }

    public function setWork(?bool $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getCommentAlu(): ?string
    {
        return $this->commentAlu;
    }

    public function setCommentAlu(?string $commentAlu): self
    {
        $this->commentAlu = $commentAlu;

        return $this;
    }

    public function getCommentDir(): ?string
    {
        return $this->commentDir;
    }

    public function setCommentDir(?string $commentDir): self
    {
        $this->commentDir = $commentDir;

        return $this;
    }

    public function getMotivation(): ?Permissions
    {
        return $this->motivation;
    }

    public function setMotivation(?Permissions $motivation): self
    {
        $this->motivation = $motivation;

        return $this;
    }

    public function getFinalDay(): ?\DateTimeInterface
    {
        return $this->finalDay;
    }

    public function setFinalDay(?\DateTimeInterface $finalDay): self
    {
        $this->finalDay = $finalDay;

        return $this;
    }
}
