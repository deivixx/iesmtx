const $ = require('jquery');


$(document).ready(function () {


    $('input[type="radio"]').click(function (e) {

        var value = $(this).val();
        if (value == "day") {
            $('label[for="absence_finalDay"]').attr("hidden", true);
            $('#absence_finalDay').attr("hidden", true);

        } else if (value == "period") {
            $('label[for="absence_finalDay"]').attr("hidden", false);
            $('#absence_finalDay').attr("hidden", false);
        }

    });

    $("#all_day_div").prependTo("#absence_hours");

    $('#all_day_check').click(function (e) {

        if ($(this).is(':checked')) {
            checkAllHours();
        }

    });

    $("input[name='absence[hours][]']").click(function (e) {
        var allChecked = allHoursChecked();
        $("#all_day_check").prop('checked', allChecked);
    });

    var allChecked = allHoursChecked();
    $("#all_day_check").prop('checked', allChecked);


    $('label[for="absence_commentAlu"]').click(function (e) {
        if ($('#absence_commentAlu').is(':visible')) {

            appendArrowIcon("down", "alu", null);
            $('#absence_commentAlu').hide();


        } else {
            appendArrowIcon("up", "alu", null);
            $('#absence_commentAlu').show();

        }
    });

    $('label[for="absence_commentDir"]').click(function (e) {
        if ($('#absence_commentDir').is(':visible')) {

            appendArrowIcon("down", null, "dir");
            $('#absence_commentDir').hide();


        } else {

            appendArrowIcon("up", null, "dir");
            $('#absence_commentDir').show();

        }
    });

    var mediaquery = window.matchMedia("(max-width: 800px)"),
        content = document.querySelector("#absence_commentAlu");

    function handleWidthChange(mediaquery) {

        if (mediaquery.matches) {
            content.style.display = "none"
            $("#absence_commentDir").hide()

            $('label[for="absence_commentAlu"]').css('cursor', 'pointer');
            $('label[for="absence_commentDir"]').css('cursor', 'pointer');

            appendArrowIcon("down", "alu", "dir");

        } else {
            content.style.display = "block"
            $("#absence_commentDir").show()

            $('label[for="absence_commentAlu"]').css('cursor', 'pointer');
            $('label[for="absence_commentDir"]').css('cursor', 'pointer');
        }

    }

    handleWidthChange(mediaquery);
    mediaquery.addListener(handleWidthChange);
});


function checkAllHours() {
    $("input[name^='absence[hours]']").each(function (checkbox) {
        if (!$(this).is(':checked')) {
            $(this).prop("checked", true);
        }
    });
}


function allHoursChecked() {

    allChecked = true;
    $("input[name^='absence[hours]']").each(function (checkbox) {
        if (!$(this).is(':checked')) {
            allChecked = false;
        }
    });

    return allChecked;
}


function appendArrowIcon(orientation, alu, dir) {

    if (alu != null) {
        $('#iconAlu').remove();
        var img1 = $('<img id="iconAlu" style="margin-left: 6px;">');

        if (orientation == "down") {
            img1.attr('src', '/img/down-arrow.png');
        } else {
            img1.attr('src', '/img/up-arrow.png');
        }

        img1.appendTo('label[for="absence_commentAlu"]');
    }

    if (dir != null) {
        $('#iconDir').remove();
        var img2 = $('<img id="iconDir" style="margin-left: 6px;">');

        if (orientation == "down") {
            img2.attr('src', '/img/down-arrow.png');
        } else {
            img2.attr('src', '/img/up-arrow.png');
        }

        img2.appendTo('label[for="absence_commentDir"]');
    }

}