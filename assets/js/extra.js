const $ = require('jquery');

require('./multi.min.js');

$(document).ready(function () {


    $("#extra_curricular_teachers").multi({
        non_selected_header: 'Listado de Profesores',
        selected_header: 'Profesores que asisten a la actividad',
        enable_search: true,
        search_placeholder: 'Busca...'
    });


    $('#all').click(function (event) {

        $(".non-selected-wrapper a.item").each(
            function () {
                var nonSelected = document.getElementsByClassName("non-selected-wrapper")[0].getElementsByClassName("item");
                for (let i = 0; i < nonSelected.length; i++) {

                    if (!nonSelected[i].getAttribute("class").includes("selected"))
                        nonSelected[i].click();
                }
            });

    });


    $('#none').click(function () {
        $(".non-selected-wrapper a.item").each(
            function (e) {
                var nonSelected = document.getElementsByClassName("non-selected-wrapper")[0].getElementsByClassName("item");
                for (let i = 0; i < nonSelected.length; i++) {

                    if (nonSelected[i].getAttribute("class").includes("selected"))
                        nonSelected[i].click();
                }
            });

    });


});



