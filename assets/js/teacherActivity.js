const $ = require('jquery');


var $addInscriptionButton = $('<button type="button" class="btn-secondary btn add_inscription_link">Add</button>');
var $newLinkLi = $('<div style="margin-top:10px;margin-bottom:10px;"></div>').append($addInscriptionButton);

jQuery(document).ready(function () {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('ul.inscriptions');

    // add a delete link to all of the existing tag form li elements
    $collectionHolder.find('li').each(function () {
        addInscriptionFormDeleteLink($(this));
    });

    $addInscriptionButton.on('click', function (e) {
        $('.prepend').show();
        // add a new tag form (see next code block)
        addInscriptionForm($collectionHolder, $newLinkLi);

    });

    $collectionHolder.append($newLinkLi);
    $collectionHolder.prepend($newLinkLi.clone(true).addClass("prepend"));

    if ($('ul.inscriptions > li').length == 0) {
        $('.prepend').hide();
    } else {
        $('.prepend').show();
    }

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

});


function addInscriptionForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li class="list-group-item"></li>').append(newForm);
    addInscriptionFormDeleteLink($newFormLi);
    $newLinkLi.before($newFormLi);
}


function addInscriptionFormDeleteLink($inscriptionFormLi) {
    var $removeFormButton = $('<button type="button" class="btn btn-danger">Delete</button>');
    $inscriptionFormLi.append($removeFormButton);

    $removeFormButton.on('click', function (e) {
        // remove the li for the tag form
        console.log($('ul.inscriptions > li').length)
        if (($('ul.inscriptions > li').length - 1) > 0) {
            $('.prepend').show();
        } else {
            $('.prepend').hide();
        }
        $inscriptionFormLi.remove();
    });
}
