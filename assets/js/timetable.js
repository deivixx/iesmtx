const $ = require('jquery');

$(document).ready(function () {


    var mediaquery = window.matchMedia("(max-width: 1431px)"),
        content = document.querySelector(".hour_desc");

    function handleWidthChange(mediaquery) {

        if (mediaquery.matches) {
            changeHourCellContent("small");

        } else {
            changeHourCellContent("big");
        }

    }

    handleWidthChange(mediaquery);
    mediaquery.addListener(handleWidthChange);


});


function changeHourCellContent(size) {

    var source = "";
    $(".hour_desc").each(function (cell) {

        if (size == "small") {
            source = "data";
        } else if (size == "big") {
            source = "original";
        }
        $(this).text($(this).attr(source));
    });
}


